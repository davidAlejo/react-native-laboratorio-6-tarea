import React, { useState, useEffect } from 'react';
import { View, Text, Button, Image, TextInput, StyleSheet, ScrollView } from 'react-native';

const ItemSetting = (props) => {
    return ( 
        <View style={styles.contenedor}>
            <View style={styles.contenedorImagen}>
                <Image style={styles.imagen} source={{uri: (props.imagen)}}/>
            </View>

            <View style={styles.texto}>
                <Text style={{ flex: 1, fontSize:25 }}>{props.texto}</Text>
            </View>
            <View style={styles.contenedorFlecha}>
                <Button
                    title=">"
                />
            </View>
        </View>
    );
}
 
const styles = StyleSheet.create({
    //Estilos lista Settings
    contenedor: {
        width:320,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: 'gray',
        paddingBottom: 10,
        marginBottom: 20,
        justifyContent:'center'
    },
    texto: {
        width:250,
        marginLeft:10,
    },
    imagen: {
        width:'100%',
        height:'100%',
    },
    contenedorImagen: {
        height:35,
        width: 35,
        justifyContent: 'center',
        alignItems: 'center'
    },
})

export default ItemSetting;