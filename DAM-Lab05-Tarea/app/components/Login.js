
import React, { useState, useEffect } from 'react';
import { StyleSheet,Text,View,TextInput,TouchableOpacity,Image, } from 'react-native';

export default function LoginView ({ navigation }) {

  const [ usuario, setUsuario ] = useState('')
  const [ contraseña, setContraseña ] = useState('')

  const Login = () => {
    //obtenemos los usuarios en el servidor iniciado con node
    fetch(
      "http://192.168.5.22:3000/users"
    )
    .then(res => res.json())
    .then(
      result => {
        let filtro = result.filter(user => user.usuario == usuario && user.contraseña == contraseña)
        if(filtro.length == 0){
          console.warn("Credenciales invalidas") 
        } else {
          navigation.navigate('Lista')
        }
      },
    )}

  return (
    <View style={styles.container}>
      <Image source={require("../img/images.png")} style={{ marginBottom:25 }}/>


      <View style={styles.inputContainer}>
        <Image style={[styles.icon, styles.inputIcon]} source={{uri: 'https://definicion.de/wp-content/uploads/2019/06/perfildeusuario.jpg'}}/>
        <TextInput style={styles.inputs}
            placeholder="Usuariol"
            onChangeText={dato1 => setUsuario(dato1)}
            underlineColorAndroid='transparent'/>
      </View>

      <View style={styles.inputContainer}>
        <Image style={[styles.icon, styles.inputIcon]} source={{uri: 'https://cdn.icon-icons.com/icons2/2387/PNG/512/public_padlock_unlock_person_people_icon_144648.png'}}/>
        <TextInput style={styles.inputs}
            placeholder="Contraseña"
            onChangeText={dato2 => setContraseña(dato2)}
            underlineColorAndroid='transparent'/>
      </View>

      <TouchableOpacity style={styles.restoreButtonContainer}>
          <Text>Se olvido su contraseña?</Text>
      </TouchableOpacity>

      <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]} onPress={Login}>
        <Text style={styles.loginText} >Login</Text>
      </TouchableOpacity>


      <TouchableOpacity style={[styles.buttonContainerFacebook, styles.loginButtonFacebook]}>
      <Text style={styles.loginText}>Login con facebook</Text>
      </TouchableOpacity>

      <TouchableOpacity style={[styles.buttonContainerGoogle, styles.loginButtonGoogle]}>
        <Text style={styles.loginTextGoogle}>Login con google</Text>
      </TouchableOpacity>
    </View>
  );

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DC2323',
  },
  inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:15,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#FFFFFF',
      flex:1,
  },
  icon:{
    width:30,
    height:30,
  },
  inputIcon:{
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: '#1C1C1C',
  },

  buttonContainerFacebook: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButtonFacebook: {
    backgroundColor: '#2F44C8',
  },

  buttonContainerGoogle: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButtonGoogle: {
    backgroundColor: '#FFFFFF',
  },

  loginText: {
    color: 'white',
  },
  loginTextGoogle: {
    color: 'black',
  },
  socialIcon:{
    color: "#FFFFFF",
    marginRight:5
  }
});
