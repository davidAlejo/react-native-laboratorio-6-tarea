import React, { useState, useEffect } from 'react';
import { View, Text, Button, Image, TextInput, StyleSheet, ScrollView } from 'react-native';
import ItemSetting from './ItemSetting'

function Settings ({ navigation }) {
    return ( 
        <ScrollView>
            <View style={styles.container}>
                
                <View style={styles.containerHijo}>
                    
                    <View style={styles.titulo}>
                        <View style={styles.botontitulo}>
                            <Button style={{ backgroundColor:'white'}} title="<"></Button>
                        </View>
                        <View style={styles.titulolabel}>
                            <Text style={{ fontSize:25 }}>Settings</Text>
                        </View>
                    </View>

                    <View style={{flex:1, alignItems:'center'}}>
                        <View style={styles.inputContainer}> 
                            <Image style={[styles.icon, styles.inputIcon]} source={{uri: 'https://img.icons8.com/ios/452/search--v1.png'}}/>
                            <TextInput style={styles.inputs}
                                placeholder="Search for a setting"
                                underlineColorAndroid='transparent'/>
                        </View>
                    </View>

                    {/* LISTA ITEMS */}
                    <View style={{flex:1, alignItems:'center', marginTop:-500}}>

                        <ItemSetting imagen="https://upload.wikimedia.org/wikipedia/commons/9/99/Sample_User_Icon.png" texto="Account"/>
                        <ItemSetting imagen="https://static.vecteezy.com/system/resources/previews/001/505/138/original/notification-bell-icon-free-vector.jpg" texto="Notifications"/>
                        <ItemSetting imagen="https://i.pinimg.com/originals/b1/2d/67/b12d6788c90cf0842d21451e00601e93.png" texto="Appearance"/>
                        <ItemSetting imagen="https://pics.freeicons.io/uploads/icons/png/11993624801548336254-512.png" texto="Privacy & Security"/>
                        <ItemSetting imagen="https://www.vhv.rs/dpng/d/115-1154749_transparent-headphones-silhouette-png-headphones-icon-png-white.png" texto="Help and Suport"/> 
                        <ItemSetting imagen="https://png.pngtree.com/png-vector/20191018/ourlarge/pngtree-question-mark-vector-icon-png-image_1824218.jpg" texto="About"/> 

                    </View>
                    
                </View>

            </View>
        </ScrollView>
        
     );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'gray',
    },
    containerHijo: {
        margin:15,
        marginLeft:15,
        marginRight:15,
        backgroundColor: 'white',
        width: 450,
        maxHeight: 5500,
        height: 800
    },


    //boton volver y titulo
    titulo: {
        flexDirection:'row',
        marginBottom:35
    },
    botontitulo:{
        alignItems:'flex-start',
        marginTop:35,
        marginLeft:25
    },
    titulolabel:{
        flex:1,
        marginLeft:-30,
        marginTop:25,
        alignItems:'center',
        justifyContent:'center',
    },
    //----------

    //buscador
    inputContainer: {
        backgroundColor: '#EAEAEA',
        borderRadius:10,
        width:370,
        height:45,
        marginBottom:15,
        flexDirection: 'row',
        alignItems:'center'
    },
    inputs:{
        height:45,
        marginLeft:16,
        borderBottomColor: '#FFFFFF',
        flex:1,
    },
    icon:{
        width:30,
        height:30,
    },
    inputIcon:{
        marginLeft:15,
        justifyContent: 'center'
    },
    //----------


})
 
export default Settings; 

