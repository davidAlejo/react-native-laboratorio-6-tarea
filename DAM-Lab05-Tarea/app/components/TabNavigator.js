import React from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

import Home from './Home'
import VideoView from './VideoView'
import Settings from './Settings'

const Tab = createMaterialBottomTabNavigator();

const TabNavigator = () => {

  return (

    <Tab.Navigator
    initialRouteName="Lista"
    tabBarOptions={{activeTintColor:'#e91e63'}}>

      <Tab.Screen 
        name="Lista" 
        component={Home} 
        options={{
        tabBarLabel:'Lista',
        tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
        ),
        }}
      />
      
      <Tab.Screen 
        name="VideoView" 
        component={VideoView}
        options={{
        tabBarLabel:'VideoView',
        tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="video" color={color} size={size} />
        ),
        }}
      />

      <Tab.Screen 
        name="Settings" 
        component={Settings}
        options={{
        tabBarLabel:'Settings',
        tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="bell" color={color} size={size} />
        ),
        }}
      />

    </Tab.Navigator>
  );
};

export default TabNavigator;
