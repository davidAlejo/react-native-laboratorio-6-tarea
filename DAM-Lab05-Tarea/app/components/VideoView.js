import React from 'react';
import { View, StyleSheet } from 'react-native';
import Video from 'react-native-video'

function VideoView () {
    return ( 
        <View style={{ flex:1 }}>
            <Video
                source={ require('../videos/spoderman.mp4') }
                style={styles.video}
                resizeMode="contain"
                controls={true}
                onEnd={() => { alert('El video ha terminado!') }}
            />
        </View>
     );
}
const styles = StyleSheet.create({
    videoContainer: {
        flex: 1,
        backgroundColor: 'black',
    },
    video: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    }
})
export default VideoView; 
